CREATE TABLE tasks (
    task_id        NUMBER
        GENERATED ALWAYS AS IDENTITY
    PRIMARY KEY,
    task_name      VARCHAR2(100) NOT NULL,
    due_date       DATE NOT NULL,
    priority_level VARCHAR2(100) NOT NULL,
    status         VARCHAR2(100) NOT NULL,
    created_by     VARCHAR2(100) NOT NULL,
    created_date   DATE NOT NULL,
    modified_by    VARCHAR2(100) NOT NULL,
    modified_date  DATE NOT NULL,
    project_id     NUMBER,
    CONSTRAINT fk_project_id FOREIGN KEY ( project_id )
        REFERENCES projects ( project_id ),
    label_id       NUMBER,
    CONSTRAINT fk_label_id FOREIGN KEY ( label_id )
        REFERENCES labels ( label_id ),
    attachment_id  NUMBER,
    CONSTRAINT fk_attachment_id FOREIGN KEY ( attachment_id )
        REFERENCES attachments ( attachment_id ),
    history_id     NUMBER,
    CONSTRAINT fk_history_id FOREIGN KEY ( history_id )
        REFERENCES task_history ( history_id )
);

CREATE TABLE users (
    user_id         NUMBER
        GENERATED ALWAYS AS IDENTITY
    PRIMARY KEY,
    username        VARCHAR2(100) UNIQUE,
    password        VARCHAR2(100) CHECK ( length(password) > 5 ),
    first_name      VARCHAR2(100) NOT NULL,
    last_name       VARCHAR2(100) NOT NULL,
    email           VARCHAR2(100) UNIQUE,
    user_position_id    NUMBER,
    CONSTRAINT fk_user_position_id FOREIGN KEY ( user_position_id )
        REFERENCES user_positions ( user_position_id ),
        task_id        NUMBER,
    CONSTRAINT fk_task_id FOREIGN KEY ( task_id )
        REFERENCES tasks ( task_id )
);

CREATE TABLE projects (
    project_id   NUMBER
        GENERATED ALWAYS AS IDENTITY
    PRIMARY KEY,
    project_name VARCHAR2(100) NOT NULL,
    description  VARCHAR2(100) NOT NULL
);

CREATE TABLE task_history (
    history_id  NUMBER
        GENERATED ALWAYS AS IDENTITY
    PRIMARY KEY,
    action      VARCHAR2(100) NOT NULL,
    action_date DATE NOT NULL
);

CREATE TABLE labels (
    label_id   NUMBER
        GENERATED ALWAYS AS IDENTITY
    PRIMARY KEY,
    label_name VARCHAR2(100) NOT NULL
);

CREATE TABLE attachments (
    attachment_id NUMBER
        GENERATED ALWAYS AS IDENTITY
    PRIMARY KEY,
    file_name     VARCHAR2(100) NOT NULL,
    file_type     VARCHAR2(100) NOT NULL,
    file_path     VARCHAR2(100) NOT NULL
);

CREATE TABLE task_assignments (
    assignment_id  NUMBER
        GENERATED ALWAYS AS IDENTITY
    PRIMARY KEY,
    assignment_date DATE NOT NULL,
    task_id        NUMBER,
    CONSTRAINT fk_tasks_task_id FOREIGN KEY ( task_id )
        REFERENCES tasks ( task_id ),
        user_id        NUMBER,
    CONSTRAINT fk_users_user_id FOREIGN KEY ( user_id )
        REFERENCES users ( user_id )
);

CREATE TABLE user_positions (
    user_position_id NUMBER
        GENERATED ALWAYS AS IDENTITY
    PRIMARY KEY,
    user_position    VARCHAR2(100) UNIQUE
);

CREATE TABLE notifications (
    notification_id      NUMBER
        GENERATED ALWAYS AS IDENTITY
    PRIMARY KEY,
    notification_content VARCHAR2(100) NOT NULL,
    notification_date    DATE NOT NULL
);

CREATE TABLE users_notifications (
    user_id         NUMBER,
    CONSTRAINT fk_user_id FOREIGN KEY ( user_id )
        REFERENCES users ( user_id ),
    notification_id NUMBER,
    CONSTRAINT fk_notification_id FOREIGN KEY ( notification_id )
        REFERENCES notifications ( notification_id )
);

CREATE TABLE comments (
    comment_id   NUMBER
        GENERATED ALWAYS AS IDENTITY
    PRIMARY KEY,
    comment_text VARCHAR2(100) NOT NULL,
    comment_date DATE NOT NULL,
    task_id        NUMBER,
    CONSTRAINT fk_tasks_table_task_id FOREIGN KEY ( task_id )
        REFERENCES tasks ( task_id )
);
