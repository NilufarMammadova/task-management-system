INSERT INTO tasks (
    task_name,
    due_date,
    priority_level,
    status,
    created_by,
    created_date,
    modified_by,
    modified_date,
    project_id,
    label_id,
    attachment_id,
    history_id
) VALUES (
    'creating table',
    TO_DATE('16-09-2023', 'dd-mm-yyyy'),
    'high',
    'in progress',
    'me',
    TO_DATE('04-09-2023', 'dd-mm-yyyy'),
    'me',
    TO_DATE('15-09-2023', 'dd-mm-yyyy'),
    1,
    1,
    1,
    1
);

INSERT INTO tasks (
    task_name,
    due_date,
    priority_level,
    status,
    created_by,
    created_date,
    modified_by,
    modified_date,
    project_id,
    label_id,
    attachment_id,
    history_id
) VALUES (
    'update table',
    TO_DATE('15-09-2023', 'dd-mm-yyyy'),
    'high',
    'in progress',
    'me',
    TO_DATE('12-09-2023', 'dd-mm-yyyy'),
    'me',
    TO_DATE('14-09-2023', 'dd-mm-yyyy'),
    2,
    2,
    2,
    2
);

COMMIT;


INSERT INTO users (
    username,
    password,
    first_name,
    last_name,
    email,
    user_position_id,
    task_id
) VALUES (
    'janebarbar',
    'jane1234',
    'jane',
    'barbar',
    'j.barbar@gmail.com',
    1,
    1
);

INSERT INTO users (
    username,
    password,
    first_name,
    last_name,
    email,
    user_position_id,
    task_id
) VALUES (
    'dianaharper',
    'diana1234',
    'diana',
    'harper',
    'd.harper@gmail.com',
    2,
    2
);

COMMIT;


INSERT INTO projects (
    project_name,
    description
) VALUES (
    'project 1',
    'creating database'
);

INSERT INTO projects (
    project_name,
    description
) VALUES (
    'project 2',
    'join tables'
);

COMMIT;


INSERT INTO task_history (
    action,
    action_date
) VALUES (
    'deleted',
    TO_DATE('03-09-2023', 'dd-mm-yyyy')
);

INSERT INTO task_history (
    action,
    action_date
) VALUES (
    'opened',
    TO_DATE('05-09-2023', 'dd-mm-yyyy')
);

COMMIT;


INSERT INTO labels ( label_name ) VALUES ( 'number 1' );

INSERT INTO labels ( label_name ) VALUES ( 'number 2' );

COMMIT;


INSERT INTO attachments (
    file_name,
    file_type,
    file_path
) VALUES (
    'file 1',
    '.xlsx',
    'folder 1'
);

INSERT INTO attachments (
    file_name,
    file_type,
    file_path
) VALUES (
    'file 2',
    '.xlsx',
    'folder 1'
);

COMMIT;


INSERT INTO task_assignments (
    assignment_date,
    task_id,
    user_id
) VALUES (
    TO_DATE('08-09-2023', 'dd-mm-yyyy'),
    1,
    1
);

INSERT INTO task_assignments (
    assignment_date,
    task_id,
    user_id
) VALUES (
    TO_DATE('09-09-2023', 'dd-mm-yyyy'),
    2,
    2
);

COMMIT;


INSERT INTO user_positions ( user_position ) VALUES ( 'database administrator' );

INSERT INTO user_positions ( user_position ) VALUES ( 'pl/sql developer' );

COMMIT;


INSERT INTO notifications (
    notification_content,
    notification_date
) VALUES (
    'modified',
    TO_DATE('02-09-2023', 'dd-mm-yyyy')
);

INSERT INTO notifications (
    notification_content,
    notification_date
) VALUES (
    'added',
    TO_DATE('03-09-2023', 'dd-mm-yyyy')
);

COMMIT;


INSERT INTO users_notifications (
    user_id,
    notification_id
) VALUES (
    1,
    1
);

INSERT INTO users_notifications (
    user_id,
    notification_id
) VALUES (
    2,
    2
);


COMMIT;


INSERT INTO comments (
    comment_text,
    comment_date,
    task_id
) VALUES (
    'modify text part',
    TO_DATE('02-09-2023', 'dd-mm-yyyy'),
    1
);

INSERT INTO comments (
    comment_text,
    comment_date,
    task_id
) VALUES (
    'adding',
    TO_DATE('03-09-2023', 'dd-mm-yyyy'),
    2
);

COMMIT;
