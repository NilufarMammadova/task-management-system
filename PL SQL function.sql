CREATE OR REPLACE FUNCTION get_notification_content(p_notification_id NUMBER)
RETURN VARCHAR2
IS
    v_notification_content VARCHAR2(100);
BEGIN
    SELECT notification_content
    INTO v_notification_content
    FROM notifications
    WHERE notification_id = p_notification_id;

    RETURN v_notification_content;
EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RETURN 'Notification not found.';
    WHEN OTHERS THEN
        RETURN 'An error occurred: ' || SQLERRM;
END;

SELECT get_notification_content(1) FROM dual;
