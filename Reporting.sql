--Reporting
--All data

CREATE OR REPLACE VIEW reporting_view AS
SELECT
    t.task_id,
    t.task_name,
    t.due_date,
    t.priority_level,
    t.status,
    t.created_by,
    t.created_date,
    t.modified_by,
    t.modified_date,
    t.project_id,
    p.project_name AS project_name,
    th.action AS history_action,
    th.action_date AS history_action_date,
    l.label_name AS label,
    a.file_name AS attachment_file_name,
    a.file_type AS attachment_file_type,
    a.file_path AS attachment_file_path,
    ta.assignment_date AS assignment_date,
    u.username AS assigned_to,
    n.notification_content AS notification_content,
    n.notification_date AS notification_date,
    c.comment_text AS comment_text,
    c.comment_date AS comment_date
FROM
    tasks t
LEFT JOIN projects p ON t.project_id = p.project_id
LEFT JOIN task_history th ON t.history_id = th.history_id
LEFT JOIN labels l ON t.label_id = l.label_id
LEFT JOIN attachments a ON t.attachment_id = a.attachment_id
LEFT JOIN task_assignments ta ON t.task_id = ta.task_id
LEFT JOIN users u ON ta.user_id = u.user_id
LEFT JOIN users_notifications un ON u.user_id = un.user_id
LEFT JOIN notifications n ON un.notification_id = n.notification_id
LEFT JOIN comments c ON t.task_id = c.task_id;



--Task Statistics by Priority:
--This view provides statistics on tasks by priority level, including the priority level and the count of tasks
--with that priority.
CREATE OR REPLACE VIEW task_statistics_by_priority AS
    SELECT
    priority_level,
    COUNT(task_id) OVER (PARTITION BY priority_level) AS task_count
FROM
    tasks;



--Task Statistics by Status:
--This view provides statistics on tasks by status, including the status and the count of tasks with that status.
CREATE OR REPLACE VIEW task_statistics_by_status AS
SELECT
    status,
    COUNT(task_id) AS task_count
FROM
    tasks
GROUP BY
    status;



--User Task Assignment Overview:
--This view shows the overview of tasks assigned to each user, including the user's username, task name, due date,
--and status.
CREATE OR REPLACE VIEW user_task_assignment_overview AS
SELECT
    u.username,
    t.task_name,
    t.due_date,
    t.status
FROM
    users u
JOIN
    task_assignments ta ON u.user_id = ta.user_id
JOIN
    tasks t ON ta.task_id = t.task_id;



--Project Task Overview:
--This view provides an overview of tasks related to each project, including the project name, task name, due date,
--and status.
CREATE OR REPLACE VIEW project_task_overview AS
    SELECT
        p.project_name,
        t.task_name,
        t.due_date,
        t.status
    FROM
             projects p
        JOIN tasks t ON p.project_id = t.project_id;
        
