CREATE OR REPLACE PACKAGE projects_package AS
    PROCEDURE insert_projects (
        p_error        OUT VARCHAR2,
        p_project_name projects.project_name%TYPE,
        p_description  projects.description%TYPE
    );

PROCEDURE update_project (
        p_error        OUT VARCHAR2,
        p_project_name projects.project_name%TYPE,
        p_description  projects.description%TYPE
    );
    
    PROCEDURE delete_project (
        p_error      OUT VARCHAR2,
         p_project_name projects.project_name%TYPE,
        p_description  projects.description%TYPE
    );
    
    PROCEDURE generate_project_report (
        p_project_id projects.project_id%TYPE,
        p_project_name projects.project_name%TYPE,
        p_description  projects.description%TYPE);

END;
/

CREATE OR REPLACE PACKAGE BODY projects_package AS

    PROCEDURE insert_projects (
        p_error        OUT VARCHAR2,
        p_project_name projects.project_name%TYPE,
        p_description  projects.description%TYPE
    ) AS
    BEGIN
       
    
--Attemp to insert into the projects table
 INSERT INTO projects (
            project_name,
            description
        ) VALUES (
            p_project_name,
            p_description
        );
        -- Commit the transaction if the insert is successful.
        COMMIT;

    EXCEPTION
        -- Handle NO_DATA_FOUND exception (e.g., when a SELECT query returns no rows).
        WHEN NO_DATA_FOUND THEN
            p_error := 'No data found.';

        -- Handle TOO_MANY_ROWS exception (e.g., when a SELECT query returns multiple rows).
        WHEN TOO_MANY_ROWS THEN
            p_error := 'Too many rows found.';

        -- Handle unique constraint violation (e.g., when trying to insert a duplicate project name).
        WHEN DUP_VAL_ON_INDEX THEN
            p_error := 'Duplicate project name.';

        -- Handle other specific exceptions as needed.

        -- Handle generic exceptions (all other exceptions not handled above).
        WHEN OTHERS THEN
            p_error := 'An error occurred: ' || sqlerrm;
            
           log_errors ('insert projects table', sqlcode, sqlerrm);
            
            ROLLBACK; -- Rollback the transaction in case of an error.
            
    END insert_projects;

    PROCEDURE update_project (
        p_error        OUT VARCHAR2,
        p_project_name projects.project_name%TYPE,
        p_description  projects.description%TYPE
    ) AS
    BEGIN
        UPDATE projects
        SET project_name = p_project_name,
            description  = p_description;
        -- Commit the transaction if the update is successful.
        COMMIT;

        -- Handle NO_DATA_FOUND exception (e.g., when the project_id does not exist).
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                p_error := 'Project not found.';

            -- Handle other specific exceptions as needed.
            -- You can add more specific exception handlers here.

            -- Handle generic exceptions (all other exceptions not handled above).
            WHEN OTHERS THEN
                p_error := 'An error occurred: ' || sqlerrm;
                ROLLBACK; -- Rollback the transaction in case of an error.
    END update_project;

PROCEDURE delete_project (
        p_error      OUT VARCHAR2,
         p_project_name projects.project_name%TYPE,
        p_description  projects.description%TYPE
    ) AS
    BEGIN
        DELETE FROM projects;
        -- Commit the transaction if the delete is successful.
        COMMIT;

        -- Handle NO_DATA_FOUND exception (e.g., when the project_id does not exist).
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                p_error := 'Project not found.';

            -- Handle other specific exceptions as needed.
            -- You can add more specific exception handlers here.

            -- Handle generic exceptions (all other exceptions not handled above).
            WHEN OTHERS THEN
                p_error := 'An error occurred: ' || sqlerrm;
                ROLLBACK; -- Rollback the transaction in case of an error.
    END delete_project;

PROCEDURE generate_project_report
(
        p_project_id projects.project_id%TYPE,
        p_project_name projects.project_name%TYPE,
        p_description  projects.description%TYPE) as
        -- Declare variables to hold cursor results
    project_id projects.project_id%TYPE;
    project_name projects.project_name%TYPE;
    description  projects.description%TYPE;

        -- Declare a cursor
        CURSOR project_cursor IS
            SELECT project_id, project_name, description
            FROM projects;

    BEGIN
        -- Open the cursor
        OPEN project_cursor;

        -- Print header for the report
        DBMS_OUTPUT.PUT_LINE('Project Report');
        DBMS_OUTPUT.PUT_LINE('----------------------------------');
        DBMS_OUTPUT.PUT_LINE('ID   | Project Name  | Description');

        -- Loop through the result set
        LOOP
            -- Fetch data into variables
            FETCH project_cursor INTO project_id, project_name, description;

            -- Exit the loop if no more rows
            EXIT WHEN project_cursor%NOTFOUND;

            -- Print project details
            DBMS_OUTPUT.PUT_LINE(project_id || ' ' || project_name || ' | ' || description);
        END LOOP;

        -- Close the cursor
        CLOSE project_cursor;
    EXCEPTION
        -- Handle exceptions as needed
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('An error occurred: ' || sqlerrm);
    END generate_project_report;

END projects_package;

BEGIN
    projects_package.generate_project_report (1, 'project 1', 'creating database');
END;



DECLARE
    v_error VARCHAR2 (1000);
BEGIN
    projects_package.insert_projects (v_error, 'project 4', 'adding details');

    DBMS_OUTPUT.PUT_LINE(v_error);
END;



CREATE OR REPLACE PACKAGE tasks_package AS
    PROCEDURE insert_tasks (
        p_error          OUT VARCHAR2,
        p_task_name      tasks.task_name%TYPE,
        p_due_date       tasks.due_date%TYPE,
        p_priority_level tasks.priority_level%TYPE,
        p_status         tasks.priority_level%TYPE,
        p_created_by     tasks.created_by%TYPE,
        p_created_date   tasks.created_date%TYPE,
        p_modified_by    tasks.modified_by%TYPE,
        p_modified_date  tasks.modified_date%TYPE
    );

PROCEDURE update_task (
        p_error          OUT VARCHAR2,
        p_task_name      tasks.task_name%TYPE,
        p_due_date       tasks.due_date%TYPE,
        p_priority_level tasks.priority_level%TYPE,
        p_status         tasks.priority_level%TYPE,
        p_modified_by    tasks.modified_by%TYPE,
        p_modified_date  tasks.modified_date%TYPE
    );

 PROCEDURE delete_task (
        p_error      OUT VARCHAR2,
        p_task_name      tasks.task_name%TYPE,
        p_due_date       tasks.due_date%TYPE,
        p_priority_level tasks.priority_level%TYPE,
        p_status         tasks.priority_level%TYPE,
        p_modified_by    tasks.modified_by%TYPE,
        p_modified_date  tasks.modified_date%TYPE
    );

PROCEDURE generate_task_report (
        p_task_id tasks.task_id%TYPE,
        p_task_name tasks.task_name%TYPE,
        p_due_date        tasks.due_date%TYPE,
        p_priority_level tasks.priority_level%TYPE,
        p_status          tasks.status%TYPE,
        p_created_by      tasks.created_by%TYPE,
        p_created_date    tasks.created_date%TYPE,
        p_modified_by     tasks.modified_by%TYPE,
        p_modified_date   tasks.modified_date%TYPE);

END;
/

CREATE OR REPLACE PACKAGE BODY tasks_package AS

    PROCEDURE insert_tasks (
        p_error          OUT VARCHAR2,
        p_task_name      tasks.task_name%TYPE,
        p_due_date       tasks.due_date%TYPE,
        p_priority_level tasks.priority_level%TYPE,
        p_status         tasks.priority_level%TYPE,
        p_created_by     tasks.created_by%TYPE,
        p_created_date   tasks.created_date%TYPE,
        p_modified_by    tasks.modified_by%TYPE,
        p_modified_date  tasks.modified_date%TYPE
    ) AS
    
    BEGIN
    
    
--Attempt to insert into the tasks table
        INSERT INTO tasks (
            task_name,
            due_date,
            priority_level,
            status,
            created_by,
            created_date,
            modified_by,
            modified_date
        ) VALUES (
            p_task_name,
            p_due_date,
            p_priority_level,
            p_status,
            p_created_by,
            p_created_date,
            p_modified_by,
            p_modified_date
        );

        -- Commit the transaction if the insert is successful.
        COMMIT;

    EXCEPTION
        -- Handle NO_DATA_FOUND exception (e.g., when a SELECT query returns no rows).
        WHEN NO_DATA_FOUND THEN
            p_error := 'No data found.';

        -- Handle TOO_MANY_ROWS exception (e.g., when a SELECT query returns multiple rows).
        WHEN TOO_MANY_ROWS THEN
            p_error := 'Too many rows found.';

        -- Handle unique constraint violation (e.g., when trying to insert a duplicate task name).
        WHEN DUP_VAL_ON_INDEX THEN
            p_error := 'Duplicate task name.';

        -- Handle other specific exceptions as needed.
        -- You can add more specific exception handlers here.

        -- Handle generic exceptions (all other exceptions not handled above).
        WHEN OTHERS THEN
            p_error := 'An error occurred: ' || sqlerrm;
            
            -- Log the error to the error_log table
                      log_errors ('insert tasks table', sqlcode, sqlerrm);

            
            ROLLBACK; -- Rollback the transaction in case of an error.
    END insert_tasks;

    PROCEDURE update_task (
        p_error          OUT VARCHAR2,
        p_task_name      tasks.task_name%TYPE,
        p_due_date       tasks.due_date%TYPE,
        p_priority_level tasks.priority_level%TYPE,
        p_status         tasks.priority_level%TYPE,
        p_modified_by    tasks.modified_by%TYPE,
        p_modified_date  tasks.modified_date%TYPE
    ) AS
    BEGIN
        UPDATE tasks
        SET task_name = p_task_name,
            due_date = p_due_date,
            priority_level = p_priority_level,
            status = p_status,
            modified_by = p_modified_by,
            modified_date = p_modified_date;
        -- Commit the transaction if the update is successful.
        COMMIT;

        -- Handle NO_DATA_FOUND exception (e.g., when the task_id does not exist).
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                p_error := 'Task not found.';

            -- Handle other specific exceptions as needed.
            -- You can add more specific exception handlers here.

            -- Handle generic exceptions (all other exceptions not handled above).
            WHEN OTHERS THEN
                p_error := 'An error occurred: ' || sqlerrm;
                ROLLBACK; -- Rollback the transaction in case of an error.
    END update_task;

PROCEDURE delete_task (
        p_error      OUT VARCHAR2,
        p_task_name      tasks.task_name%TYPE,
        p_due_date       tasks.due_date%TYPE,
        p_priority_level tasks.priority_level%TYPE,
        p_status         tasks.priority_level%TYPE,
        p_modified_by    tasks.modified_by%TYPE,
        p_modified_date  tasks.modified_date%TYPE
    ) AS
    BEGIN
        DELETE FROM tasks;
        -- Commit the transaction if the delete is successful.
        COMMIT;

        -- Handle NO_DATA_FOUND exception (e.g., when the task_id does not exist).
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                p_error := 'Task not found.';

            -- Handle other specific exceptions as needed.
            -- You can add more specific exception handlers here.

            -- Handle generic exceptions (all other exceptions not handled above).
            WHEN OTHERS THEN
                p_error := 'An error occurred: ' || sqlerrm;
                ROLLBACK; -- Rollback the transaction in case of an error.
    END delete_task;


  PROCEDURE generate_task_report (
  p_task_id tasks.task_id%TYPE,
        p_task_name tasks.task_name%TYPE,
        p_due_date        tasks.due_date%TYPE,
        p_priority_level tasks.priority_level%TYPE,
        p_status          tasks.status%TYPE,
        p_created_by      tasks.created_by%TYPE,
        p_created_date    tasks.created_date%TYPE,
        p_modified_by     tasks.modified_by%TYPE,
        p_modified_date   tasks.modified_date%TYPE) IS
        -- Declare variables to hold cursor results
        task_id tasks.task_id%TYPE;
        task_name       tasks.task_name%TYPE;
        due_date        tasks.due_date%TYPE;
        priority_level tasks.priority_level%TYPE;
        status          tasks.status%TYPE;
        created_by      tasks.created_by%TYPE;
        created_date    tasks.created_date%TYPE;
        modified_by     tasks.modified_by%TYPE;
        modified_date   tasks.modified_date%TYPE;

        -- Declare a cursor
        CURSOR task_cursor IS
            SELECT
            task_id,
                   task_name,
                   due_date,
                   priority_level,
                   status,
                   created_by,
                   created_date,
                   modified_by,
                   modified_date
            FROM tasks;

    BEGIN
        -- Open the cursor
        OPEN task_cursor;

        -- Print header for the report
        DBMS_OUTPUT.PUT_LINE('Task Report');
        DBMS_OUTPUT.PUT_LINE('--------------------------------------------------------------------------------');
        DBMS_OUTPUT.PUT_LINE('ID   | Task Name      | Due Date    | Priority | Status | Created By | Modified By');

        -- Loop through the result set
        LOOP
            -- Fetch data into variables
            FETCH task_cursor INTO 
            task_id,
                                   task_name,
                                   due_date,
                                   priority_level,
                                   status,
                                   created_by,
                                   created_date,
                                   modified_by,
                                   modified_date;

            -- Exit the loop if no more rows
            EXIT WHEN task_cursor%NOTFOUND;

            -- Print task details
            DBMS_OUTPUT.PUT_LINE(task_id || ' ' || task_name || ' | ' || due_date || ' | ' || priority_level ||
                                 ' | ' || status || ' | ' || created_by || ' | ' || modified_by);
        END LOOP;

        -- Close the cursor
        CLOSE task_cursor;
    EXCEPTION
        -- Handle exceptions as needed
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('An error occurred: ' || sqlerrm);
    END generate_task_report;

END tasks_package;



CREATE OR REPLACE PACKAGE users_package AS
    PROCEDURE insert_users (
        p_error      OUT VARCHAR2,
        p_username   users.username%TYPE,
        p_password   users.password%TYPE,
        p_first_name users.first_name%TYPE,
        p_last_name  users.last_name%TYPE,
        p_email      users.email%TYPE
    );

PROCEDURE update_user (
        p_error      OUT VARCHAR2,
        p_username   users.username%TYPE,
        p_password   users.password%TYPE,
        p_first_name users.first_name%TYPE,
        p_last_name  users.last_name%TYPE,
        p_email      users.email%TYPE
    );

PROCEDURE delete_user (
        p_error      OUT VARCHAR2,
        p_username   users.username%TYPE,
        p_password   users.password%TYPE,
        p_first_name users.first_name%TYPE,
        p_last_name  users.last_name%TYPE,
        p_email      users.email%TYPE
    );

PROCEDURE generate_user_report (
        p_user_id      users.user_id%TYPE,
        p_username     users.username%TYPE,
        p_first_name   users.first_name%TYPE,
        p_last_name    users.last_name%TYPE,
        p_email        users.email%TYPE
);

END;
/

CREATE OR REPLACE PACKAGE BODY users_package AS

    PROCEDURE insert_users (
        p_error      OUT VARCHAR2,
        p_username   users.username%TYPE,
        p_password   users.password%TYPE,
        p_first_name users.first_name%TYPE,
        p_last_name  users.last_name%TYPE,
        p_email      users.email%TYPE
    ) AS
    BEGIN
        
        -- Attempt to insert into the users table
        INSERT INTO users (
            username,
            password,
            first_name,
            last_name,
            email
        ) VALUES (
            p_username,
            p_password,
            p_first_name,
            p_last_name,
            p_email
        );

        -- Commit the transaction if the insert is successful.
        COMMIT;

    EXCEPTION
        -- Handle NO_DATA_FOUND exception (e.g., when a SELECT query returns no rows).
        WHEN NO_DATA_FOUND THEN
            p_error := 'No data found.';

        -- Handle TOO_MANY_ROWS exception (e.g., when a SELECT query returns multiple rows).
        WHEN TOO_MANY_ROWS THEN
            p_error := 'Too many rows found.';

        -- Handle unique constraint violation (e.g., when trying to insert a duplicate username).
        WHEN DUP_VAL_ON_INDEX THEN
            p_error := 'Duplicate username.';

        -- Handle other specific exceptions as needed.
        -- Handle generic exceptions (all other exceptions not handled above).
        WHEN OTHERS THEN
            p_error := 'An error occurred: ' || sqlerrm;
            
               log_errors ('insert users table', sqlcode, sqlerrm);

            ROLLBACK; -- Rollback the transaction in case of an error.
    END insert_users;

    PROCEDURE update_user (
        p_error      OUT VARCHAR2,
        p_username   users.username%TYPE,
        p_password   users.password%TYPE,
        p_first_name users.first_name%TYPE,
        p_last_name  users.last_name%TYPE,
        p_email      users.email%TYPE
    ) AS
    BEGIN
        UPDATE users
        SET username = p_username,
            password = p_password,
            first_name = p_first_name,
            last_name = p_last_name,
            email = p_email;
        -- Commit the transaction if the update is successful.
        COMMIT;

        -- Handle NO_DATA_FOUND exception (e.g., when the user_id does not exist).
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                p_error := 'User not found.';

            -- Handle other specific exceptions as needed.
            -- Handle generic exceptions (all other exceptions not handled above).
            WHEN OTHERS THEN
                p_error := 'An error occurred: ' || sqlerrm;
                ROLLBACK; -- Rollback the transaction in case of an error.
    END update_user;

PROCEDURE delete_user (
        p_error      OUT VARCHAR2,
        p_username   users.username%TYPE,
        p_password   users.password%TYPE,
        p_first_name users.first_name%TYPE,
        p_last_name  users.last_name%TYPE,
        p_email      users.email%TYPE
    ) AS
    BEGIN
        DELETE FROM users;
        -- Commit the transaction if the delete is successful.
        COMMIT;

        -- Handle NO_DATA_FOUND exception (e.g., when the user_id does not exist).
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                p_error := 'User not found.';

            -- Handle other specific exceptions as needed.
            -- You can add more specific exception handlers here.

            -- Handle generic exceptions (all other exceptions not handled above).
            WHEN OTHERS THEN
                p_error := 'An error occurred: ' || sqlerrm;
                ROLLBACK; -- Rollback the transaction in case of an error.
    END delete_user;

    -- You can add more procedures and functions as needed here.

PROCEDURE generate_user_report (
        p_user_id      users.user_id%TYPE,
        p_username     users.username%TYPE,
        p_first_name   users.first_name%TYPE,
        p_last_name    users.last_name%TYPE,
        p_email        users.email%TYPE
) IS
        -- Declare variables to hold cursor results
        user_id    users.user_id%TYPE;
        username     users.username%TYPE;
        first_name   users.first_name%TYPE;
        last_name    users.last_name%TYPE;
        email        users.email%TYPE;

        -- Declare a cursor
        CURSOR user_cursor IS
            SELECT  user_id, username, first_name, last_name, email
            FROM users;

    BEGIN
        -- Open the cursor
        OPEN user_cursor;

        -- Print header for the report
        DBMS_OUTPUT.PUT_LINE('User Report');
        DBMS_OUTPUT.PUT_LINE('-------------------------------------------');
        DBMS_OUTPUT.PUT_LINE('ID   | Username     | First Name  | Last Name   | Email');

        -- Loop through the result set
        LOOP
            -- Fetch data into variables
            FETCH user_cursor INTO user_id, username, first_name, last_name, email;

            -- Exit the loop if no more rows
            EXIT WHEN user_cursor%NOTFOUND;

            -- Print user details
            DBMS_OUTPUT.PUT_LINE(user_id || ' ' || username || ' | ' || first_name || ' | ' || last_name || ' | ' || email);
        END LOOP;

        -- Close the cursor
        CLOSE user_cursor;
    EXCEPTION
        -- Handle exceptions as needed
        WHEN OTHERS THEN
            DBMS_OUTPUT.PUT_LINE('An error occurred: ' || sqlerrm);
    END generate_user_report;

END users_package;


BEGIN
    users_package.generate_user_report (1, 'nilymammad', 'Nilufar', 'Mammadova', 'n.mammadly@gmail.com');
END;
